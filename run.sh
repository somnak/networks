docker exec -it cli bash
CORE_PEER_LOCALMSPID=CooconMSP
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/coocon.kshrd.com.kh/users/Admin@coocon.kshrd.com.kh/msp
CORE_PEER_ADDRESS=peer0.coocon.kshrd.com.kh:7051
peer channel create -o orderer.kshrd.com.kh:7050 -c mychannel -f /etc/hyperledger/configtx/channel.tx
peer channel join -b mychannel.block


# Peer1 of Coocon organization
CORE_PEER_LOCALMSPID=CooconMSP
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/coocon.kshrd.com.kh/users/Admin@coocon.kshrd.com.kh/msp
CORE_PEER_ADDRESS=peer1.coocon.kshrd.com.kh:7051
peer channel join -b mychannel.block

# Peer0 of Webcash organization
CORE_PEER_LOCALMSPID=WebcashMSP
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/webcash.kshrd.com.kh/users/Admin@webcash.kshrd.com.kh/msp
CORE_PEER_ADDRESS=peer0.webcash.kshrd.com.kh:7051
peer channel join -b mychannel.block

# Peer1 of Webcash organization
CORE_PEER_LOCALMSPID=WebcashMSP
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/webcash.kshrd.com.kh/users/Admin@webcash.kshrd.com.kh/msp
CORE_PEER_ADDRESS=peer1.webcash.kshrd.com.kh:7051
peer channel join -b mychannel.block

# Set Environment Variable to Peer0 in Coocon Organization
CORE_PEER_LOCALMSPID=CooconMSP
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/coocon.kshrd.com.kh/users/Admin@coocon.kshrd.com.kh/msp
CORE_PEER_ADDRESS=peer0.coocon.kshrd.com.kh:7051

# update the Anchor Peer
peer channel update -o orderer.kshrd.com.kh:7050 -c mychannel -f /etc/hyperledger/configtx/CooconMSPanchors.tx

# Set Environment Variable to Peer0 in Webcash Organization
CORE_PEER_LOCALMSPID=WebcashMSP
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/webcash.kshrd.com.kh/users/Admin@webcash.kshrd.com.kh/msp
CORE_PEER_ADDRESS=peer0.webcash.kshrd.com.kh:7051

# update the Anchor Peer
peer channel update -o orderer.kshrd.com.kh:7050 -c mychannel -f /etc/hyperledger/configtx/WebcashMSPanchors.tx

# docker exec -it cli bash

CORE_PEER_LOCALMSPID=CooconMSP
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/coocon.kshrd.com.kh/users/Admin@coocon.kshrd.com.kh/msp
CORE_PEER_ADDRESS=peer0.coocon.kshrd.com.kh:7051
peer channel create -o orderer.kshrd.com.kh:7050 -c mychannel -f /etc/hyperledger/configtx/channel.tx
peer chaincode install -n mycar1 -v 2.0 -p github.com/kshrdsmartcontract/go/mycar -l golang


# Peer1 of Coocon organization
CORE_PEER_LOCALMSPID=CooconMSP
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/coocon.kshrd.com.kh/users/Admin@coocon.kshrd.com.kh/msp
CORE_PEER_ADDRESS=peer1.coocon.kshrd.com.kh:7051
peer chaincode install -n mycar1 -v 2.0 -p github.com/kshrdsmartcontract/go/mycar -l golang

# Peer0 of Webcash organization
CORE_PEER_LOCALMSPID=WebcashMSP
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/webcash.kshrd.com.kh/users/Admin@webcash.kshrd.com.kh/msp
CORE_PEER_ADDRESS=peer0.webcash.kshrd.com.kh:7051
peer chaincode install -n mycar1 -v 2.0 -p github.com/kshrdsmartcontract/go/mycar -l golang

# Peer1 of Webcash organization
CORE_PEER_LOCALMSPID=WebcashMSP
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/webcash.kshrd.com.kh/users/Admin@webcash.kshrd.com.kh/msp
CORE_PEER_ADDRESS=peer1.webcash.kshrd.com.kh:7051
peer chaincode install -n mycar1 -v 2.0 -p github.com/kshrdsmartcontract/go/mycar -l golang

CORE_PEER_LOCALMSPID=CooconMSP
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/coocon.kshrd.com.kh/users/Admin@coocon.kshrd.com.kh/msp
CORE_PEER_ADDRESS=peer0.coocon.kshrd.com.kh:7051
peer chaincode instantiate -o orderer.kshrd.com.kh:7050 -C mychannel -n mycar1 -l golang -v 2.0 -c '{"Args":[""]}' -P "OR ('CooconMSP.member','WebcashMSP.member')"
peer chaincode invoke -o orderer.kshrd.com.kh:7050 -C mychannel -n mycar1  -c '{"Args":["initLedger"]}'
