package main

import (
	"bytes"
	"encoding/json"
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	sc "github.com/hyperledger/fabric/protos/peer"
)

type ChainCode struct {
}

type mycar struct {
	SerialNumber string
	Maker        string
	ModelName    string
	Color        string
	Owner        owner
}
type owner struct {
	Name    string
	Age     string
	Address string
}

func (cc *ChainCode) Init(stub shim.ChaincodeStubInterface) sc.Response {
	return shim.Success(nil)
}

func (cc *ChainCode) Invoke(stub shim.ChaincodeStubInterface) sc.Response {
	function, args := stub.GetFunctionAndParameters()
	if function == "initLedger" {
		return cc.initLedger(stub)
	} else if function == "changeOwner" {
		return cc.changeOwner(stub, args)
	} else if function == "queryCar" {
		return cc.queryCar(stub, args)
	} else if function == "createCar" {
		return cc.createCar(stub, args)
	} else if function == "carHistory" {
		return cc.carHistory(stub, args)
	} else if function == "getQueryResult" {
		return cc.getQueryResult(stub, args)
	}
	return shim.Error("Invalid Smart Contract function name.")
}
func (cc *ChainCode) queryCar(stub shim.ChaincodeStubInterface, args []string) sc.Response {

	var A string // Entities
	var err error

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting name of the person to query")
	}

	A = args[0]

	// Get the state from the ledger
	Avalbytes, err := stub.GetState(A)
	if err != nil {
		jsonResp := "{\"Error\":\"Failed to get state for " + A + "\"}"
		return shim.Error(jsonResp)
	}

	if Avalbytes == nil {
		jsonResp := "{\"Error\":\"Nil amount for " + A + "\"}"
		return shim.Error(jsonResp)
	}

	jsonResp := "{\"Record" + string(Avalbytes) + "\"}"
	fmt.Printf("Query Response:%s\n", jsonResp)
	return shim.Success(Avalbytes)
}
func (cc *ChainCode) initLedger(stub shim.ChaincodeStubInterface) sc.Response {
	cars := []mycar{
		mycar{Maker: "TOYOTA", SerialNumber: "1F-123230", ModelName: "Ferari", Color: "Black", Owner: owner{Name: "SomNak", Age: "12", Address: "pp"}},
		mycar{Maker: "TOYOTA", SerialNumber: "1F-123231", ModelName: "BMW", Color: "Black", Owner: owner{Name: "SomNak", Age: "12", Address: "pp"}},
		mycar{Maker: "TOYOTA", SerialNumber: "1F-123232", ModelName: "Tesla", Color: "Black", Owner: owner{Name: "SomNak", Age: "12", Address: "pp"}},
		mycar{Maker: "TOYOTA", SerialNumber: "1F-123233", ModelName: "KIA", Color: "Black", Owner: owner{Name: "SomNak", Age: "12", Address: "pp"}},
		mycar{Maker: "TOYOTA", SerialNumber: "1F-123234", ModelName: "Rover", Color: "Black", Owner: owner{Name: "SomNak", Age: "12", Address: "pp"}},
		mycar{Maker: "TOYOTA", SerialNumber: "1F-123235", ModelName: "Ferari", Color: "Black", Owner: owner{Name: "SomNak", Age: "12", Address: "pp"}},
		mycar{Maker: "TOYOTA", SerialNumber: "1F-123236", ModelName: "Ferari", Color: "Black", Owner: owner{Name: "SomNak", Age: "12", Address: "pp"}},
		mycar{Maker: "TOYOTA", SerialNumber: "1F-123237", ModelName: "Ferari", Color: "Black", Owner: owner{Name: "SomNak", Age: "12", Address: "pp"}},
		mycar{Maker: "TOYOTA", SerialNumber: "1F-123238", ModelName: "Ferari", Color: "Black", Owner: owner{Name: "SomNak", Age: "12", Address: "pp"}},
	}
	i := 0
	for i < len(cars) {
		fmt.Println("i is ", i)
		id := cars[i].SerialNumber
		carAsBytes, _ := json.Marshal(cars[i])
		stub.PutState(id, carAsBytes)
		fmt.Println("Added", cars[i])
		i = i + 1
	}
	return shim.Success(nil)
}
func (cc ChainCode) createCar(stub shim.ChaincodeStubInterface, args []string) sc.Response {
	if len(args) != 7 {
		return shim.Error("Incorrect number of arguments. Expecting 7")
	}
	mycar := mycar{SerialNumber: args[0], Maker: args[1], ModelName: args[2], Color: args[3],
		Owner: owner{Name: args[4], Age: args[5], Address: args[6]}}
	id := mycar.SerialNumber
	carAsBytes, _ := json.Marshal(mycar)
	stub.PutState(id, carAsBytes)
	return shim.Success(nil)
}
func (cc *ChainCode) carHistory(stub shim.ChaincodeStubInterface, args []string) sc.Response {
	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}
	SerialNumber := args[0]
	resultsIterator, err := stub.GetHistoryForKey(SerialNumber)
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()

	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"Key\":")
		buffer.WriteString("\"")
		buffer.WriteString(queryResponse.GetTxId())
		buffer.WriteString("\"")

		buffer.WriteString(", \"Record\":")
		buffer.WriteString(string(queryResponse.Value))
		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")

	fmt.Printf("- Car History:\n%s\n", buffer.String())

	return shim.Success(buffer.Bytes())
}
func (cc *ChainCode) changeOwner(stub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 4 {
		return shim.Error("Incorrect number of arguments. Expecting 4")
	}

	carAsBytes, _ := stub.GetState(args[0])
	car := mycar{}
	owner := owner{args[1], args[2], args[3]}

	json.Unmarshal(carAsBytes, &car)
	car.Owner = owner

	carAsBytes, _ = json.Marshal(car)
	stub.PutState(args[0], carAsBytes)

	return shim.Success(nil)
}
func (cc *ChainCode) getQueryResult(stub shim.ChaincodeStubInterface, args []string) sc.Response {
	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}
	fmt.Printf("- getQueryResultForQueryString queryString:\n%s\n", args[0])
	resultsIterator, err := stub.GetQueryResult(args[0])
	defer resultsIterator.Close()
	if err != nil {
		return shim.Success(nil)
	}

	var buffer bytes.Buffer
	buffer.WriteString("[")
	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return shim.Success(nil)
		}

		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"Key\":")
		buffer.WriteString("\"")
		buffer.WriteString(queryResponse.Key)
		buffer.WriteString("\"")
		buffer.WriteString(", \"Record\":")
		// Record is a JSON object, so we write as-is
		buffer.WriteString(string(queryResponse.Value))
		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")
	fmt.Printf("- getQueryResultForQueryString queryResult:\n%s\n", buffer.String())
	return shim.Success(buffer.Bytes())
}

func main() {
	err := shim.Start(new(ChainCode))
	if err != nil {
		fmt.Printf("Error creating new Smart Contract: %s", err)
	}
}
