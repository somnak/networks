#!/bin/bash
# 1. Update the apt package index:
sudo apt-get update

# 2. Install packages to allow apt to use repository over HTTPS:
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common

# 3. Add Docker's official GPG key:
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo apt-key fingerprint 0EBFCD88

# 4. Setup the stable repository.
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

# Downloading the golang file
wget https://dl.google.com/go/go1.9.3.linux-amd64.tar.gz

# Extract it in home directory
tar -xvf go1.9.3.linux-amd64.tar.gz

mkdir $HOME/gopath

# set GOPATH and GOROOT
echo "export GOPATH=$HOME/gopath" > ~/.bashrc
echo "export GOROOT=$HOME/go" > ~/.bashrc
echo "export GOBIN=$HOME/go/bin" > ~/.bashrc
echo "export PATH=$PATH:/$GOROOT/bin" > ~/.bashrc
source ~/.bashrc
sudo apt-get install docker-ce

sudo curl -L https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

# Check the Docker Compose version
docker-compose --version

sudo usermod -a -G docker $USER

curl -sSL http://bit.ly/2ysbOFE | bash -s 1.3.0
echo "export PATH=$PATH:$HOME/fabric-samples/bin">~/.bashrc
source ~/.bashrc
mkdir -p $GOPATH/src/kshrd/KSHRD-BLOCKCHAIN-NETWORK/networks
touch crypto-config.yaml configtx.yaml docker-compose.yaml
