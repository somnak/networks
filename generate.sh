#!/bin/bash
# Create the config directory to store the channel-artifacts
cd $GOPATH/src/kshrd/KSHRD-BLOCKCHAIN-NETWORK/networks
mkdir config
export FABRIC_CFG_PATH=$PWD
# Generate the Genesis Block
configtxgen -profile KSHRDCooconWebcashOrdererGenesis -outputBlock ./config/genesis.block
# Generate channel configuration transaction
configtxgen -profile CooconWebcashOrgChannel -outputCreateChannelTx ./config/channel.tx -channelID mychannel
# peer transaction for Coocon Organization
configtxgen -profile CooconWebcashOrgChannel -outputAnchorPeersUpdate ./config/CooconMSPanchors.tx -channelID mychannel -asOrg CooconMSP

# generate anchor peer transaction for Webcash Organization
configtxgen -profile CooconWebcashOrgChannel -outputAnchorPeersUpdate ./config/WebcashMSPanchors.tx -channelID mychannel -asOrg WebcashMSP